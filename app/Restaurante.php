<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurante extends Model
{
    protected $fillable = ['nome','cpf','cnpj','razao_social','endereco'];
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'usuario_restaurante';
}
