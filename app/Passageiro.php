<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passageiro extends Model
{
    protected $fillable = ['nome','cpf','endereco','id_pagamento','id_origem'];
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'usuario_passageiro';
}
