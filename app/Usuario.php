<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = ['nome','cpf'];
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'usuarios';
}
