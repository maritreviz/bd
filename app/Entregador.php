<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entregador extends Model
{
    protected $fillable = ['nome','cpf'];
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'usuario_entregador';
}
