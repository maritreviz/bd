<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motorista extends Model
{
    protected $fillable = ['nome','cpf','cnh','placa','modelo','cor'];
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'usuario_motorista';
}
