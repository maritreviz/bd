<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Entregador;
use App\Motorista;
use App\Passageiro;
use App\Restaurante;

class CadastroController extends Controller
{
    public function index()
    {
       // $usuario = Usuario::orderBy('cpf', 'desc')->paginate(10);
       // return view('usuario.index',['usuario' => $usuario]);
        return view('usuario.index');
    }
  
    public function create()
    {
        return view('usuario.create');
    }
  
    public function store(Request $request)
    {
       
        $usuario = new Usuario([
        'nome' => $request->get('nome'),
        'cpf'=> $request->get('cpf')]);
        $usuario->save();

        if($request->get('tipo_usuario') == 1){
            $entregador = new Entregador([
            'nome' => $request->get('nome'),
            'cpf'=> $request->get('cpf')]);
            $entregador->save();
        } else if($request->get('tipo_usuario') == 2){
            $motorista = new Motorista([
            'nome' => $request->get('nome'),
            'cpf'=> $request->get('cpf'),
            'cnh' => $request->get('cnh')]);
            $motorista->save();
        }else if($request->get('tipo_usuario') == 3){
            $passageiro = new Passageiro([
            'nome' => $request->get('nome'),
            'cpf'=> $request->get('cpf')]);
            $passageiro->save();
        }else if($request->get('tipo_usuario') == 4){
            $restaurante = new Restaurante([
            'nome' => $request->get('nome'),
            'cpf'=> $request->get('cpf')]);
            $restaurante->save();
        }

         return redirect('/usuario')->with('success', 'User has been added');
        
       
    }
  
    public function show()
    {
        
        $restaurante = Restaurante::orderBy('cpf', 'desc')->paginate(10);
            return view('usuario.show',['restaurante' => $restaurante]);
    }
  
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('products.edit',compact('product'));
    }
  
    public function update(ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->name        = $request->name;
        $product->description = $request->description;
        $product->quantity    = $request->quantity;
        $product->price       = $request->price;
        $product->save();
        return redirect()->route('products.index')->with('message', 'Product updated successfully!');
    }
  
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('products.index')->with('alert-success','Product hasbeen deleted!');
    }
}
