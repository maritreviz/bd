<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/usuario', 'CadastroController@index');
Route::post('/usuario', 'CadastroController@store');
Route::resource('usuario', 'CadastroController');
Route::get('/pesquisa_restaurantes', 'CadastroController@show');