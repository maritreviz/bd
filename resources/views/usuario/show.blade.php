<!DOCTYPE html>
<html>
<head>
	<title>Cadastro</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <style>
     .container {
         width: 100vw;
         height: 100vh;
         display: flex;
         flex-direction: row;
         justify-content: center;
         align-items: center
     }
     .box {
         width: 300px;
         height: 300px;
         background: #fff;
     }
     
     body {
        margin: 0px;
 }
 </style>
</head>
<body>
  <div class="container">
   <form>
    @csrf
    <div class="form-row">
      <div class="form-group col-md-4">
        <div class="box">
          <h5>Restaurantes Parceiros</h5>
          <table class="table">    
            <thead class="thead-light">
              <tr>    
                <th scope="col">Restaurantes</th>
                <th scope="col">Endereço</th>
              </tr>
            </thead>
            <tbody>
             @foreach ($restaurante as $r) <tr>

              <td>{{ $r->nome }}</td>
              <td>{{ $r->endereco }}</td>
              </tr> @endforeach
              
            </tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</form>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>